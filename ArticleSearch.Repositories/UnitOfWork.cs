﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Repositories
{
    public interface IUnitOfWork
    {
        GenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

        IEnumerable<TEntity> SQLQuery<TEntity>(string sql, params object[] parameters);

        ObjectResult<TEntity> ExecuteFunction<TEntity>(string functionName, params ObjectParameter[] parameters);

        Task<bool> SaveAsync();

        bool Save();

        GenericRepository<Category> GetCategoryRepository();
        GenericRepository<Dimension> GetDimensionRepository();
        GenericRepository<ProductFamily> GetProductFamilyRepository();
        GenericRepository<ProductFamilyDimensionMapping> GetProductFamilyDimensionMappingRepository();
        GenericRepository<Supplier> GetSupplierRepository();
        GenericRepository<Article> GetArticleRepository();
        GenericRepository<User> GetUserRepository();





    }

    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private readonly ArticleSearchEntities _context = new ArticleSearchEntities();
        private bool _disposed;

        public GenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return new GenericRepository<TEntity>(_context);
        }

        public IEnumerable<TEntity> SQLQuery<TEntity>(string sql, params object[] parameters)
        {
            return _context.Database.SqlQuery<TEntity>(sql, parameters);
        }

        public ObjectResult<TEntity> ExecuteFunction<TEntity>(string functionName, params ObjectParameter[] parameters)
        {
            return ((IObjectContextAdapter)this._context).ObjectContext.ExecuteFunction<TEntity>(functionName, parameters);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<bool> SaveAsync()
        {
            int succussCode = await _context.SaveChangesAsync();
            if (succussCode > 0)
                return true;
            else
                return false;
        }

        public bool Save()
        {
            try
            {
                int succussCode = _context.SaveChanges();
                if (succussCode > 0)
                    return true;
                else
                    return false;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                        
                    }
                }
                return false;
            }
           
        }






        //Table Repositories

        #region Category
        private GenericRepository<Category> _categoryRepository;
        private GenericRepository<Category> CategoryRepository
        {
            get { return _categoryRepository ?? (_categoryRepository = new GenericRepository<Category>(_context)); }
        }
        public GenericRepository<Category> GetCategoryRepository()
        {
            return CategoryRepository;
        }


        #endregion

        #region Dimension
        private GenericRepository<Dimension> _dimensionRepository;
        private GenericRepository<Dimension> DimensionRepository
        {
            get { return _dimensionRepository ?? (_dimensionRepository = new GenericRepository<Dimension>(_context)); }
        }
        public GenericRepository<Dimension> GetDimensionRepository()
        {
            return DimensionRepository;
        }

        #endregion

        #region ProductFamily
        private GenericRepository<ProductFamily> _productFamilyRepository;
        private GenericRepository<ProductFamily> ProductFamilyRepository
        {
            get { return _productFamilyRepository ?? (_productFamilyRepository = new GenericRepository<ProductFamily>(_context)); }
        }
        public GenericRepository<ProductFamily> GetProductFamilyRepository()
        {
            return ProductFamilyRepository;
        }

        #endregion

        #region ProductFamilyDimensionMapping
        private GenericRepository<ProductFamilyDimensionMapping> _productFamilyDimensionMappingRepository;
        private GenericRepository<ProductFamilyDimensionMapping> ProductFamilyDimensionMappingRepository
        {
            get { return _productFamilyDimensionMappingRepository ?? (_productFamilyDimensionMappingRepository = new GenericRepository<ProductFamilyDimensionMapping>(_context)); }
        }
        public GenericRepository<ProductFamilyDimensionMapping> GetProductFamilyDimensionMappingRepository()
        {
            return ProductFamilyDimensionMappingRepository;
        }

        #endregion

        #region Article
        private GenericRepository<Article> _articleRepository;
        private GenericRepository<Article> ArticleRepository
        {
            get { return _articleRepository ?? (_articleRepository = new GenericRepository<Article>(_context)); }
        }
        public GenericRepository<Article> GetArticleRepository()
        {
            return ArticleRepository;
        }

        #endregion

        #region supplyer
        private GenericRepository<Supplier> _supplierRepository;
        private GenericRepository<Supplier> SupplierRepository
        {
            get { return _supplierRepository ?? (_supplierRepository = new GenericRepository<Supplier>(_context)); }
        }
        public GenericRepository<Supplier> GetSupplierRepository()
        {
            return SupplierRepository;
        }

        #endregion

        #region user
        private GenericRepository<User> _userRepository;
        private GenericRepository<User> UserRepository
        {
            get { return _userRepository ?? (_userRepository = new GenericRepository<User>(_context)); }
        }
        public GenericRepository<User> GetUserRepository()
        {
            return UserRepository;
        }
        #endregion




    }
    
}
