﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Repositories
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal ArticleSearchEntities _dataContext;
        internal DbSet<TEntity> _dbSet;

        public GenericRepository(ArticleSearchEntities dataContext)
        {
            _dataContext = dataContext;
            _dbSet = dataContext.Set<TEntity>();
        }

        public IQueryable<TEntity> GetQueryable()
        {
            return _dataContext.Set<TEntity>().AsNoTracking();
        }

        public IQueryable<TEntity> GetQueryable<TProperty>(Expression<Func<TEntity, TProperty>> path)
        {
            return _dataContext.Set<TEntity>().Include(path).AsNoTracking();
        }

        public IQueryable<TEntity> GetQueryableWithTrackingEnabled<TProperty>(Expression<Func<TEntity, TProperty>> path)
        {
            return _dataContext.Set<TEntity>().Include(path);
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return orderBy != null ? orderBy(query).ToList() : query.ToList();
        }

        public async virtual Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return orderBy != null ? await orderBy(query).ToListAsync() : await query.ToListAsync();
        }

        public virtual TEntity GetById(object id)
        {
            return _dbSet.Find(id);
        }

        public async virtual Task<TEntity> GetByIdAsync(object id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> filter = null)
        {
            return null != filter ? _dbSet.FirstOrDefault(filter) : _dbSet.FirstOrDefault();
        }

        public async virtual Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return null != filter ? await _dbSet.FirstOrDefaultAsync(filter) : await _dbSet.FirstOrDefaultAsync();
        }

        public virtual TEntity Insert(TEntity entity)
        {
            return _dbSet.Add(entity);
        }

        public virtual IEnumerable<TEntity> InsertRange(IEnumerable<TEntity> entities)
        {
            return _dbSet.AddRange(entities);
        }

        public virtual TEntity Delete(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            return Delete(entityToDelete);
        }

        public virtual TEntity Delete(TEntity entityToDelete)
        {
            return _dbSet.Remove(entityToDelete);
        }

        public virtual IEnumerable<TEntity> DeleteRange(IEnumerable<TEntity> entitiesToDelete)
        {
            return _dbSet.RemoveRange(entitiesToDelete);
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> filter)
        {
            var entities = _dbSet.Where(filter).AsEnumerable();
            foreach (var entity in entities)
            {
                _dbSet.Remove(entity);
            }
        }

        public virtual TEntity Update(TEntity entityToUpdate)
        {
            var entity = _dbSet.Attach(entityToUpdate);
            _dataContext.Entry(entityToUpdate).State = EntityState.Modified;
            return entity;
        }

    }
}
