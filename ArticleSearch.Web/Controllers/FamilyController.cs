﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArticleSearch.Web.Models;
using ArticleSearch.Service.ProductFamilyServices;
using ArticleSearch.Service.DimentionServices;
using ArticleSearch.Service.ProductFamilyDimensionMappingServices;
using ArticleSearch.Repositories;
using ArticleSearch.Data;

namespace ArticleSearch.Web.Controllers
{
    public class FamilyController : Controller
    {
        
        #region Declarations
        private readonly IProductFamilyService _productFamilyService;
        private readonly IDimentionService _dimentionService;
        private readonly IProductFamilyDimensionMappingService _productFamilyDimensionMappingService; 
        #endregion

        #region Ctor
		 public FamilyController(IUnitOfWork unitOfWork, IProductFamilyService productFamilyService, IDimentionService dimentionService, IProductFamilyDimensionMappingService productFamilyDimensionMappingService)
        {
            _dimentionService = dimentionService;
            _productFamilyService = productFamilyService;
            _productFamilyDimensionMappingService = productFamilyDimensionMappingService;
        }

 
	#endregion  

        #region Methods

         public ActionResult Create()
         {
             return View("Index");
         }

         [HttpPost]
         public ActionResult Create(FamilyInfoModel model)
         {
             if (ModelState.IsValid)
             {
                 var familyInfo = new ProductFamily
                 {
                     Id = model.Id,
                     DepartmentId = 1, //temprary take electricle Department
                     FamilyName = model.FamilyName,
                     FamilyNumber = model.FamilyNumber,
                     CategoryId = model.CategoryId,
                     Status = 0,
                     StandardReference = model.StandardReference,
                     CreatedBy = "", //for now we set it empty
                     CreatedDate = System.DateTime.Now
                 };
                 var familyId = _productFamilyService.InsertFamilies(familyInfo);
                 if (familyId == 0)
                 {
                     ViewBag.error = "family name or Number not Entered";
                     return View();
                 }

                 //dimension (the Dimension will be more, so need to create array and each dim store in table)
                 List<string> DimNames = new List<string>();
                 DimNames.Add("Dim_ Test 1");
                 DimNames.Add("Dim_ Test 2");
                 DimNames.Add("Dim_ Test 3");
                 DimNames.Add("Dim_ Test 4");
                 DimNames.Add("Dim_ Test 5");

                 model.DimensionName = DimNames;
                 foreach (var dim in model.DimensionName)
                 {
                     var count = 0;
                     var dims = new Dimension { Name = dim, Unit = "" };
                     var dimId = _dimentionService.InsertDimention(dims);
                     // inserting in mapping table
                     var familyDim_mapping = new ProductFamilyDimensionMapping { ProductFamilyId = familyId, DimensionId = Convert.ToInt16(dimId) };
                     if (_productFamilyDimensionMappingService.InsertProductFamilyDimensionMapping(familyDim_mapping))
                     {
                         count++;
                     }
                     if (count == model.DimensionName.Count)
                     {
                         ViewBag.success = "Family Inserted SuccessFully";
                     }
                 }
             }
             return View(model);
         }

         public ActionResult Demo()
         {
             return View();
         }
        [HttpPost]
         public ActionResult Demo(DemoModel demoModel)
         {
             var list = demoModel.Names;
             return View("Index");
         }

         #endregion
    }
}
