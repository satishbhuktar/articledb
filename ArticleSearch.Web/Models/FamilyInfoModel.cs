﻿using ArticleSearch.Data;
using ArticleSearch.Web.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArticleSearch.Web.Models
{
    [FluentValidation.Attributes.Validator(typeof(FamilyInfoValidator))]
    public class FamilyInfoModel
    {
        public int DepartmentId { get; set; }

        public int Id { get; set; }

        [Display(Name = "Family Number")]
        public string FamilyNumber { get; set; }

        [Display(Name = "Family Name")]
        public string FamilyName { get; set; }

        [Display(Name = "Standard Reference")]
        public string StandardReference { get; set; }

        [Display(Name = "Category Id")]
        public int CategoryId { get; set; }

        public Nullable<byte> Status { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public virtual Category Category { get; set; }

        [Display(Name = "Dimension Name")]
        public List<string> DimensionName { get; set; }

    }
}