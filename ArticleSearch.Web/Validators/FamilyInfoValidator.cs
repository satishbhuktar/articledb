﻿using ArticleSearch.Web.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArticleSearch.Web.Validators
{
    public partial class FamilyInfoValidator : AbstractValidator<FamilyInfoModel>
    {
        public FamilyInfoValidator()
        {
            RuleFor(x => x.FamilyNumber)
                .NotNull();

            RuleFor(x => x.FamilyName)
                .NotNull();
                
            RuleFor(x => x.CategoryId)
                .NotNull();

            RuleFor(x => x.DimensionName)
                .NotNull();
        }

    }
}