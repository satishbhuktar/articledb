﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ArticleSearch.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


             // RentalProperties/
            routes.MapRoute(
                name: "FamilyCreate",
                url: "Family",
                defaults: new
                {
                    controller = "Family",
                    action = "Create",
                    id = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
               // defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

           
        }
    }
}