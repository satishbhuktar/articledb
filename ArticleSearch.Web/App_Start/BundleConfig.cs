﻿using System.Web;
using System.Web.Optimization;

namespace ArticleSearch.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {


            //------------------------script bundles-------------------------------
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*", //it take all file start with unobtrusive name. e.g. jquery.unobtrusive-ajax, jquery.unobtrusive-ajax.min
                        "~/Scripts/jquery.validate*"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            //.css
            bundles.Add(new StyleBundle("~/CommonCSS").Include(
                "~/Content/*.css",
             
              "~/Content/bootstrap-3.3.4-dist/css/*.css",
               "~/Content/font/*.css",
               "~/Content/font-awesome-4.3.0/css/*.css",
              "~/Content/kendo/2016.1.412/*.css"
            ));

            //.js
            bundles.Add(new ScriptBundle("~/Scripts").Include(
              
                "~/Scripts/*.js"
             
                 ));
            bundles.Add(new ScriptBundle("~/Scripts/kendo/2016.1.412").Include(
              "~/Scripts/kendo/2016.1.412/*.js"
                 ));

         




        }
    }
}