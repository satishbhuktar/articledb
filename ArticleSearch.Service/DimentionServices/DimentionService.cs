﻿using ArticleSearch.Data;
using ArticleSearch.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.DimentionServices
{
   public class DimentionService:IDimentionService
    {
      private IUnitOfWork _unitOfWork;
      public DimentionService(IUnitOfWork unitOfWork)
      {
          _unitOfWork = unitOfWork;
      }

      public int InsertDimention(Dimension dimention)
      {
          int dimensionId = 0;
          bool flag;
          if (dimention != null)
          {
              // checking for allready exist. 
              var data = _unitOfWork.GetDimensionRepository().Get(s => s.Name.Equals(dimention.Name)).FirstOrDefault();// FamilyId.Equals(dimention.FamilyId)).FirstOrDefault();
              if (data != null) //allready exist
              {
                  dimensionId = data.Id;
              }
              else
              {
                  _unitOfWork.GetDimensionRepository().Insert(dimention);
                  _unitOfWork.Save();
                  dimensionId = dimention.Id;
              }
              

          }
          else //if empty
          {
              dimensionId = 0;
          }
          return dimensionId;
      }
      public IEnumerable<Dimension> GetAll()
      {
          return _unitOfWork.GetDimensionRepository().Get();
      }


    }
}
