﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.ProductFamilyDimensionMappingServices
{
    public interface IProductFamilyDimensionMappingService
    {
        bool InsertProductFamilyDimensionMapping(ProductFamilyDimensionMapping productFamilyDimension);
        IEnumerable<ProductFamilyDimensionMapping> GetAll();
    }
}
