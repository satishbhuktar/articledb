﻿using ArticleSearch.Data;
using ArticleSearch.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.CategoryServices
{
  public  class CategoryService:ICategoryService
    {
       private IUnitOfWork _unitOfWork;
       public CategoryService(IUnitOfWork unitOfWork)
      {
          _unitOfWork = unitOfWork;
      }

      public bool InsertCategory(Category category)
      {
          bool flag;
          if (category != null)
          {
              // checking for allready exist category name. not checking for subcatagory all ready Exist.
              var rowCount = _unitOfWork.GetCategoryRepository().Get(s => s.Name.Equals(category.Name)).Count();
              if (rowCount > 0) //allready exist
              {
                  flag = false;
              }
              else
              {
                  _unitOfWork.GetCategoryRepository().Insert(category);
                  _unitOfWork.Save();
                  flag = true;
              }
              _unitOfWork.GetCategoryRepository().Insert(category);
              _unitOfWork.Save();
              flag = true;

          }
          else //if empty
          {
              flag = false;
          }
          return flag;
      }
    }
}
