﻿using ArticleSearch.Data;
using ArticleSearch.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.ArticleServices
{
    public class ArticleService : IArticleService
    {
      private IUnitOfWork _unitOfWork;
      public ArticleService(IUnitOfWork unitOfWork)
      {
          _unitOfWork = unitOfWork;
      }

      public int InsertArticle(Article article)
      {
          int articleId = 0;
          if (article != null)
          {
              var data = _unitOfWork.GetArticleRepository().Get(s => s.ArticleNo.Equals(article.ArticleNo)).FirstOrDefault();
              if (data != null) //allready exist
              {
                  articleId = data.Id;
              }
              else
              {
                  _unitOfWork.GetArticleRepository().Insert(article);
                  _unitOfWork.Save();
                  articleId = article.Id;
              }

          }
          else //if empty
          {
              articleId = 0;
          }
          return articleId;
      }

      public IEnumerable<Article> GetAll()
      {
          return _unitOfWork.GetArticleRepository().Get();
      }
    }
}
