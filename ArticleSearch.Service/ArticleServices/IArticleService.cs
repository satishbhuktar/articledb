﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.ArticleServices
{
    public interface IArticleService
    {
        int InsertArticle(Article article);
        IEnumerable<Article> GetAll();
    }
}
