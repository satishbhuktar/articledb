﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArticleSearch.Repositories;
using ArticleSearch.Data;
namespace ArticleSearch.Service.UserServices
{
    public class UserService : IUserService
    {
       private IUnitOfWork _unitOfWork;
       public UserService(IUnitOfWork unitOfWork)
       {
           _unitOfWork = unitOfWork;
       }

       public User GetUserByUserNameAndPassword(string username, string password )
       {
           var user = _unitOfWork.GetUserRepository().Get().Where(a => a.UserName == username && a.Password == password).FirstOrDefault();
           return user;
       }
    }
}
