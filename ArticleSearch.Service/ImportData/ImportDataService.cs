﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArticleSearch.Repositories;
using ArticleSearch.Service.CategoryServices;
using ArticleSearch.Service.DimentionServices;
using ArticleSearch.Service.ImportData;
using ArticleSearch.Service.ProductFamilyDimensionMappingServices;
using ArticleSearch.Service.ProductFamilyServices;
using System.IO;
using System.Data;
using ArticleSearch.Data;
using ArticleSearch.Service.SupplyerServices;
using System.Xml.XPath;
using ArticleSearch.Service.ArticleServices;
using System.Globalization;

namespace ArticleSearch.Service.ImportData
{
    public class ImportDataService : IImportDataService
    {
        #region PrivateMember

        private IUnitOfWork _unitOfWork;
        private ICategoryService _categoryService;
        private IDimentionService _dimentionService;
        private IProductFamilyDimensionMappingService _productFamilyDimensionMappingService;
        private IProductFamilyService _productFamilyService;
        private IArticleService _articleService;
        private ISupplyerService _supplyerService;

        #endregion


        #region Constructor
        public ImportDataService(IUnitOfWork unitOfWork, ICategoryService categoryService, IDimentionService dimentionService,
            IProductFamilyDimensionMappingService productFamilyDimensionMappingService, IProductFamilyService productFamilyService, IArticleService articleService, ISupplyerService supplyerService)// 
        {
            _unitOfWork = unitOfWork;
            _categoryService = categoryService;
            _dimentionService = dimentionService;
            _productFamilyDimensionMappingService = productFamilyDimensionMappingService;
            _productFamilyService = productFamilyService;
            _supplyerService = supplyerService;
            _articleService = articleService;
        }
        #endregion

        

        #region Method


        public void ImportFamilyData(string path)
        {
            //throw new NotImplementedException();
            path = @"D:\Lotus_Notes\ArticleSearch\Code\ArticleSearch\ArticleSearch.Web\Uploaded_Files\export_electrical_articles_families_.xlsx";
            string extension = Path.GetExtension(path);
            if (extension != ".XLSX" || extension != ".xlsx")
            {
                //return null;
            }
            #region satish code for .xlsx
            bool hasHeader = false;
            var pck = new OfficeOpenXml.ExcelPackage();
            var stream = System.IO.File.OpenRead(path);
            pck.Load(stream);

            var ws = pck.Workbook.Worksheets.ElementAt(0);
            DataTable tbl_dim = new DataTable();
            DataTable tbl_family = new DataTable();

           // var classMaster = new ClassMaster();

            #region processing worksheet
            //dim only
            foreach (var firstRowCell in ws.Cells[1, 4, 1, ws.Dimension.End.Column]) //adding cols
            {
                tbl_dim.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
            }
            var startRow_ = hasHeader ? 1 : 2;
            for (int rowNum = startRow_; rowNum <= ws.Dimension.End.Row; rowNum++)//adding rows
            {
                var wsRow = ws.Cells[rowNum, 4, rowNum, ws.Dimension.End.Column];
                DataRow row = tbl_dim.Rows.Add();
                foreach (var cell in wsRow)
                {
                    row[cell.Start.Column - 4] = cell.Text;//-4 becoz start from col 4
                }

            }
            //
            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column]) //adding cols ws.Dimension.End.Column only 3 cols
            {
                tbl_family.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
            }
            var startRow = hasHeader ? 1 : 2;
            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)//adding rows
            {
                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];// ws.Dimension.End.Column: read only first 3 col
                DataRow row = tbl_family.Rows.Add();
                foreach (var cell in wsRow)
                {
                    row[cell.Start.Column - 1] = cell.Text;
                }

            }
            

             insert_DimDataTable(tbl_dim);
             insert_FamilyDataTable(tbl_family);
            



            #endregion





            #endregion

        }


        public void insert_FamilyDataTable(DataTable dt)
        {
            
            //defining Table Objects
            var family = new ProductFamily();
            var familyDimMapping = new ProductFamilyDimensionMapping();
            //var category = new Category();
            //var dimention = new Dimension();
            //var familyDimensionMapping = new ProductFamilyDimensionMapping();
            int familyId = 0;
            foreach (DataRow row in dt.Rows)
            {//inserting
                //family
                family.FamilyNumber= row[0].ToString();
                family.FamilyName = row[1].ToString();
                family.StandardReference = row[2].ToString();
                family.CategoryId = 4;
                family.Status = 0;
                family.CreatedBy = "";
                family.CreatedDate = null;
              familyId =  _productFamilyService.InsertFamilies(family);
                        for (int i = 3; i < row.ItemArray.Count(); i++) //Dim reading start from cell 3
                        {
                            if (row[i].ToString() != "" )
                            {
                                familyDimMapping.ProductFamilyId = familyId; //familyDimMapping.ProductFamily.Id;
                                familyDimMapping.DimensionId = _dimentionService.GetAll().Where(t => t.Name.Contains(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(row[i].ToString().ToLower().Trim()))).FirstOrDefault().Id; //get DimId by Dim name.
                                _productFamilyDimensionMappingService.InsertProductFamilyDimensionMapping(familyDimMapping);
                            }
                    
                        }
                
            }
        }

        public void insert_DimDataTable(DataTable dt)
        {

            //defining Table Objects
            //var category = new Category();
            var dimention = new Dimension();
             var family = new ProductFamily();
            //var familyDimensionMapping = new ProductFamilyDimensionMapping();

          //  int i_ = 0;
            foreach (DataRow row in dt.Rows)
            {
                //inserting Dimensions
                for (int i = 0; i < row.ItemArray.Count(); i++)
                {
                    if (row[i].ToString() != "" )
                    {
                        dimention.Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(row[i].ToString().ToLower().Trim());//first lter uppercase; row[i].ToString().ToLower();
                        dimention.Unit = "";
                        dimention.FamilyId = family.Id;
                        _dimentionService.InsertDimention(dimention);
                    }
                    
                }
                
            }
        }

        //public void insert_Family_dim_mapping(DataTable dt)
        //{
        //    var familyDimMapping = new ProductFamilyDimensionMapping();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        familyDimMapping.ProductFamilyId = _productFamilyService.
        //    }
        //}


        public bool ImportAricleData(string path)
        {

           // throw new NotImplementedException();
            path = @"D:\Lotus_Notes\ArticleSearch\Code\ArticleSearch\ArticleSearch.Web\Uploaded_Files\export_electrical_articles.xlsx";
            string extension = Path.GetExtension(path);
            if (extension != ".XLSX" || extension != ".xlsx")
            {
                //return null;
            }
            #region satish code for .xlsx
            bool hasHeader = false;
            var pck = new OfficeOpenXml.ExcelPackage();
            var stream = System.IO.File.OpenRead(path);
            pck.Load(stream);

            var ws = pck.Workbook.Worksheets.ElementAt(0);
            DataTable tbl_Article = new DataTable();
          

            #region processing worksheet

            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column]) 
            {
                tbl_Article.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
            }
            var startRow = hasHeader ? 1 : 2;
            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)//adding rows
            {
                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                DataRow row = tbl_Article.Rows.Add();
                foreach (var cell in wsRow)
                {
                    row[cell.Start.Column - 1] = cell.Text;
                }

            }

            insert_ArticleDataTable(tbl_Article);

            return true;


            #endregion





            #endregion
        }

        public void insert_ArticleDataTable(DataTable dt)
        {
            var article = new Article();
            var supplier = new Supplier();
            int supplierId = 0;
            foreach (DataRow row in dt.Rows)
            {
                
                //insert supplyer in supplyer table
                supplier.Name = row[5].ToString();
                supplier.Designation = row[6].ToString();
                supplierId = _supplyerService.InsertSupplyer(supplier);
                //insert in article table
                article.ArticleNo = row[0].ToString();
                article.ProductFamilyId = _productFamilyService.GetAll().Where(t => t.FamilyNumber.Equals(row[1].ToString())).FirstOrDefault().Id;
                article.ArticleName = row[3].ToString();
                article.SG = Convert.ToInt32(row[4]);
                article.SupplierId = supplierId;
                article.DrawingNo = row[8].ToString();
                article.SpecificCompany = row[9].ToString();
                article.UsedAt = row[11].ToString();
                article.SC =row[12].ToString();
                //converting articles dim into dt to send to XML Method
              // DataTable XmlDt = new DataTable();
             //  DataRow XmlRow = XmlDt.NewRow();
             //  XmlDt.TableName = "xml";
                int index=0;
                StringBuilder XmlString = new StringBuilder();
                XmlString = XmlString.Append("<dims>");
                //taking dimensions
                var mappingData =  _productFamilyDimensionMappingService.GetAll().Where(t => t.ProductFamilyId.Equals(article.ProductFamilyId));
                int[] DimArr =new int[mappingData.Count()];
                int j=0;
                foreach (var item in mappingData)
                   {
                       if (item.ProductFamilyId== article.ProductFamilyId)
                       {
                           DimArr[j] = item.DimensionId;
                           j++;
                       }
                   }
                //
                int c = 0;
                for (int i = 13; i < row.ItemArray.Count(); i++) //Dim reading start from cell 3
                        {
                            
                            if (row[i].ToString() != "" )
                            {

                                XmlString.Append("<Dimension Id=" + " \" "+ DimArr[c] + " \" "+   " value= "+ " \" " + row[i].ToString() +" \" " + "/>");/* article.ProductFamilyId*/
                               // XmlDt.Columns.Add(index.ToString());
                               // XmlRow[index]= row[i];
                                index++;
                                c++;
                            }
                        }
               XmlString.Append("</dims>");
             //  XmlDt.Rows.Add(XmlRow);
            //   convert_ArticleDimDataToXml(XmlDt);
               article.ArticleDimensonData = XmlString.ToString();  //insert XML of Dims of article sheet <xml
               _articleService.InsertArticle(article);
                //
                
              
            }
        }
        public string convert_ArticleDimDataToXml(DataTable dt)
        {
            //MemoryStream str = new MemoryStream();
            //dt.WriteXml(str, true);
            //str.Seek(0, SeekOrigin.Begin);
            //StreamReader sr = new StreamReader(str);
            //string xmlstr;
            //xmlstr = sr.ReadToEnd();
            //return (xmlstr);
            //
            //XPathDocument result;
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    dt.WriteXml(ms);
            //    ms.Position = 0;
            //    result = new XPathDocument(ms);
            //}
            //
            string result;
            using (StringWriter sw = new StringWriter())
            {
                dt.WriteXml(sw);
                result = sw.ToString();
            }
            return "";
        }





        #endregion





        
    }
}
