﻿using ArticleSearch.Data;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Model
{
  public  class _productFamily
    {
        public int Id { get; set; }
        public string FamilyNumber { get; set; }
        public string FamilyName { get; set; }
        public string StandardReference { get; set; }
        public int CategoryId { get; set; }
        public Nullable<byte> Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<ProductFamilyDimensionMapping> ProductFamilyDimensionMappings { get; set; }
        public virtual ICollection<ArticleData_temp> Articles { get; set; }
    }
}


