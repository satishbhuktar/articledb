﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ArticleDataField
    {
        public int Id { get; set; }
        public string ArticleNo { get; set; }
        public int ProductFamilyId { get; set; }
        public string ProductFamilName { get; set; }
        public string ProductFamilyNo { get; set; }
        public string ArticleName { get; set; }
        public int SG { get; set; }
        public string SupplierName { get; set; }
        public string SupplierDesignation { get; set; }

        public string StandardReference { get; set; }
        public string DrawingNo { get; set; }
        public string SpecificCompany { get; set; }
        public string UsedAt { get; set; }
        public string SC { get; set; }
        public string ArticleXMLdata { get; set; }
        public IEnumerable<string> ArticleDimensonColoms { get; set; }
        public IEnumerable<string> ArticleDimensonData { get; set; }
       
    }
}
