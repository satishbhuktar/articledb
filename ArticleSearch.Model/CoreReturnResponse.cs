﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class CoreReturnResponse
    {
        public int Id { get; set; }
        public string ReturnString { get; set; }
        public bool Succeeded { get; set; }
    }
}
